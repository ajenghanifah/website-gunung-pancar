-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2020 at 10:33 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gunungpancar_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', 'admin123', 'admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chart`
--

CREATE TABLE `tbl_chart` (
  `id_chart` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tanggal` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `create_chart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `title`, `image`, `description`) VALUES
(9, '1', 'gallery-1.jpg', '1'),
(10, '2', 'gallery-2.jpg', '2'),
(11, '3', 'gallery-3.jpg', '3'),
(12, '4', 'gallery-4.jpg', '4'),
(13, '5', 'gallery-5.png', '5'),
(14, '6', 'gallery-6.jpg', '6'),
(15, '7', 'gallery-7.jpg', '7'),
(16, '8', 'gallery-8.jpg', '8'),
(17, '9', 'gallery-9.jpg', '9'),
(18, '10', 'gallery-10.jpg', '10'),
(19, '11', 'gallery-11.png', '11'),
(20, '12', 'gallery-12.jpg', '12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jadwal`
--

CREATE TABLE `tbl_jadwal` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_produk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jadwal`
--

INSERT INTO `tbl_jadwal` (`id`, `tanggal`, `id_produk`) VALUES
(1, '2020-06-06', 2),
(4, '2020-05-30', 4),
(5, '2020-07-11', 2),
(6, '2020-06-13', 2),
(7, '2020-07-04', 2),
(8, '2020-10-10', 17),
(9, '2020-10-03', 16),
(10, '2020-11-27', 15),
(11, '2020-11-28', 14),
(12, '2020-10-10', 13),
(13, '2020-07-11', 12),
(14, '2020-07-11', 11),
(15, '2020-10-03', 10),
(16, '2020-11-07', 9),
(17, '2020-07-11', 8),
(18, '2020-07-11', 7),
(19, '2020-07-11', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori_produk`
--

CREATE TABLE `tbl_kategori_produk` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `biaya_tambahan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori_produk`
--

INSERT INTO `tbl_kategori_produk` (`id`, `nama_kategori`, `deskripsi`, `biaya_tambahan`) VALUES
(1, 'Paket Camping Individu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(2, 'Paket Camping Rombongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(3, 'Paket Team Building', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(4, 'Paket Team Tracking', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(5, 'Tiket Masuk', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(7, 'Camping Ground', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_konfirmasi`
--

CREATE TABLE `tbl_konfirmasi` (
  `id_konfirmasi` int(11) NOT NULL,
  `id_chart` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `konfirmasi_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `title`, `deskripsi`, `image`, `createdAt`, `updatedAt`) VALUES
(1, '5 Fasilitas di Gunung Pancar, dari Hutan Pinus hingga Pemandian Air Panas', 'Jakarta - detikers sedang mencari spot wisata alam yang tidak jauh dari Jakarta? Gunung Pancar bisa menjadi rekomendasi untuk melakukan kegiatan luar yang berkesan.\r\n\r\nGunung Pancar yang terletak di kabupaten bogor Jawa Barat ini, sudah lumayan terkenal. Setiap akhir pekan, banyak orang yang datang ke sana untuk merasakan kesegaran alamnya.\r\n\r\nNah, penasaran seperti apa Gunung Pancar, dan di dalamnya ada apa saja? Berikut aktifitas yang bisa dinikmati detikers bila berkunjung ke sana, dikutip dari berbagai sumber:\r\n\r\n1. Berfoto\r\n\r\nTempat ini sangat cocok bagi fotografer, foto prewedding, dan milenial yang gemar membagikan aktifitasnya di sosial media. Pasalnya banyak spot, dan angle yang bisa diambil di kawasan hutan pinus tersebut. Ditambah sinar matahari yang tidak bisa masuk lantaran pepohonannya menjulang tinggi, dengan itu cahaya yang dihasilkan tidak begitu kuat atau menyilaukan.\r\n\r\n2. Bersantai di Hammock\r\n\r\nMemasang hammock di antara kedua batang pohon pinus yang besar, kemudian beristirahat, dan rileks. Dengan menikmati suasana alam yang masih asri, diiringi nyanyian burung adalah hal yang paling menarik.\r\n\r\n3. Outbond\r\n\r\nBila ingin merasakan tantangan seru bisa mencoba dengan menikmati arena outbond. Lakukan hal tersebut bersama teman atau keluarga.\r\n\r\nBerbagai macam permainan yang ada di sana, di antaranya paint ball, flying fox, dan sebagainya.\r\n\r\n4. Berkemah\r\n\r\nBagi detikers yang ingin lebih lama lagi di sana, bisa dengan berkemah untuk lebih merasakan sensasi dingin di malam hari. Gunung Pancar yang merupakan perbukitan hijau, dengan deretan pohon pinus membuat suasana yang segar dan membuat betah.\r\n\r\nBerkemah di sana bisa dengan membawa tenda sendiri dari rumah, atau bisa juga menyewa tenda yang sudah disediakan di sana.\r\n\r\n5. Pemandian Air Panas\r\n\r\nDi Gunung Pancar juga menyediakan pemandian air panas bagi detikers yang ingin berendam sambil berelaksasi. Harga untuk pemandian tersebut cukup dengan membayar sekitar Rp 25.000.\r\n\r\nNah, itulah aktivitas yang detikers bisa nikmati bila datang ke Gunung pancar. Jangan lupa menyiapkan uang tiket masuk kawasan tersebut, sekitar Rp 5.000 untuk weekday dan Rp 7.500 untuk weekend. Untuk biaya parkirnya sebesar Rp 5.000 untuk motor dan Rp 10.000 untuk mobil.\r\n\r\nAyo ke Gunung Pancar weekend ini!', 'news-1.jpg', '2020-05-21 09:32:17', '2020-05-21 18:18:23'),
(2, 'Wisata Gunung Pancar dengan Alam yang Sangat Asri, Cocok Sebagai Pelepas Penat', 'Jakarta Wisata Gunung Pancar ternyata ada di Bogor. Bogor memang dikenal sebagai kota yang banyak menyimpan potensi alam yang perlu dilestarikan.\r\n\r\nBanyaknya pegunungan di wilayah Bogor ini, menjadikan kawasan ini memiliki hawa yang sejuk. Maka tak heran kalau banyak orang Bogor dan sekitarnya yang berwisata ke kota ini bersama keluarganya.\r\n\r\nTak hanya sejuk, Kota Bogor juga memiliki beragam objek wisata yang patut untuk dikunjungi di akhir pekan. Salah satunya adalah objek wisata Gunung Pancar. Tempat wisata Gunung Pancar ini memang jauh dari pusat kota Bogor. Namun tak ada salahnya untuk memilih berwisata ke Gunung Pancar untuk melepaskan penat selama berkegiatan seminggu penuh.\r\n\r\nNah buat kamu yang memiliki rencana untuk menghabiskan akhir pekan di daerah Bogor, wajib memasukkan wisata Gunung Pancar ini ke dalam daftar destinasimu.\r\n\r\nTempat Wisata yang Murah Meriah\r\nKawasan Taman Wisata Alam (TWA) gunung Pancar yang terletak di Desa Karang Tengah, Kecamatan Babakan Madang, Kabupaten Bogor ini bisa kamu jadikan sebagai tempat liburan yang tepat bila kamu hanya memiliki dana pas-pasan.\r\n\r\nNamun perlu diperhatikan, saat berkunjung ke sini, kamu perlu menjaga tata krama. Seperti gunung atau hutan lainnya, yang banyak menyimpan misteri.\r\n\r\nMelakukan Kegiatan Seru di Gunung Pancar\r\nWisata Gunung Pancar pertama yang bisa kamu nikmati adalah dengan mencoba beragam kegiatam outdoor. Ya, Gunung Pancar memang sering dipakai untuk kegiatan outbound, camping, gathering, pelatihan kepemimpinan, dan kegiatan outdoor lainnya. Nah, kalau kamu mencari tempat camping di Bogor yang tidak jauh dari Jakarta dan ramah akan keluarga, makaGunung Pancar inilah salah satu pilihan terbaiknya.\r\n\r\nTempat ini dijadikan sebagai pilihan yang tepat bagi kamu yang ingin berkemah. Bagaimana tidak, hutan pinus Gunung Pancar Sentul ini menyediakan berbagai fasilitas dan paket-paket yang lengkap.\r\n\r\nTak heran, kalau kawasan ini menjadi camping ground terbaik di Bogor karena selalu dirawat oleh para petugas kebersihan. Maka keasrian dan kenyaman kamu dalam melakukan kegiatan di sini selalu terjaga.\r\n\r\nWisata Budaya di Kaki Gunung Pancar\r\nWisata Gunung Pancar yang bisa kamu nikmati lainnya adalah dengan menyelami budaya masyarakat di sini. Ya, selain menikmati indahnya alam di Gunung Pancar, tak ada salahnya untuk mengenal budaya setempat.\r\n\r\nDi kawasan wisata Gunung Pancar ini terdapat wisata budaya yang bisa kamu coba. Selain itu, di kawasan Gunung Pancar ini kamu juga bisa berziarah ke makan keramat yang ada. Di puncaknya sendiri, kamu bisa melihat makam-makam keramat yang sudah ada sejak zaman dahulu kala.\r\n\r\nYa, terdapat banyak tokoh legenda Jawa yang dimakamkan di wilayah ini. Misalnya saja Raden Lawulung, Ki Mas Bungsu, Raden Surya Kencana, hingga Sunan Kalijaga. Bila kamu datang di waktu yang tepat, maka kamu bisa menyaksikan pertunjukan seni dan upacara keagamaan di daerah ini.\r\n\r\nMenikmati Indahnya Alam Sambil Nongkrong\r\nWisata Gunung Pancar yang bisa dinikmati sambil nongkrong. Ya, siapa sangka di dalam hutan kamu tidak bisa menikmatinya sambil bercengkrama ditemani dengan sajian kopi dan camilan lainnya.\r\n\r\nDi tengah hutan pinus ini, kamu bisa bersantai sambil ditemani camilan yang tersedia di salah satu café di sini. Ya, di tengah hutan pinus yang tenang dan nyaman ini, kamu bisa menikmati sajian ala café pada umumnya seperi ngopi atau ngeteh.\r\n\r\nSudah terbayang kan, bagaimana nikmatnya nongkrong di tengah hutan pinus seperti ini. Tak hanya ditemani dengan camilan yang nikmat, di sini kamu juga bisa mendengar suara-suara kumbang beserta binatang hutan lainnya. Makin seru pastinya.\r\n\r\nNikmati Hangatnya Pemandian Air Panas\r\nSiapa sangka, di wisata Gunung Pancar hanya bisa menikmati indahnya alam saja. Seperti yang sudah diketahui, kalau di Bogor sendiri memiliki beberapa tempat pemandian air panas.\r\n\r\nNah, di kawasan Gunung Pancar ini juga ada. Ya, setelah kamu melakukan berbagai kegiatan menantang, kamu bisa langsung menyegarkan diri di pemandian air hangat Giritirta. Di pemandian air hangat ini kamu bisa memilih mau berendam di kolam umum atau pribadi.\r\n\r\nBisa Dijadikan Alternatif untuk Resepsi Pernikahan\r\nSelain dijadikan tempat untuk melakukan berbagai kegiatan outdoor yang menantang, Gunung Pancar ini juga sering digunakan para calon pasangan pengantin untuk mengambil gambar pranikah atau biasa disebut dengan prewedding. Ya, hutan pinus Sentul, merupakan panggilan untuk kawasan wisata ini.\r\n\r\nSelain kawasannya yang indah dan cocok untuk berswafoto, di sini kamu juga bisa melakukan prosesi pernikahan dengan nuansa alam yang asri. Gunung Pancar ini bisa kamu jadikan alternatif untuk menggelar acara pernikahan. Pohon-pohon pinus di gunung Pancar ini tak kalah indahnya dengan yang ada di Lembang.\r\n\r\nYa, memilih hutan pinus untuk prosesi pernikahan, merupakan salah satu tema garden wedding yang sedang populer. Konsep pernikahan rustic outdoor diartikan sebagai konsep pernikahan dengan gaya pedesaan yang elok, karena berkaitan dengan alam dan sederhana. Konsepnya yang menawarkan suasana asri dan alami ini akan memberikan sensasi dan kenangan tersendiri.', 'gallery-1.jpg', '2020-05-21 09:33:22', '2020-05-21 18:20:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `id` int(50) NOT NULL,
  `nama_produk` varchar(200) NOT NULL,
  `price` varchar(200) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `deskripsi` text NOT NULL,
  `id_kategori` int(50) NOT NULL,
  `des_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`id`, `nama_produk`, `price`, `image`, `deskripsi`, `id_kategori`, `des_price`) VALUES
(2, 'Camping Non Tenda', '95000', 'asasas', 'Free Tiket Masuk, Toilet Sharing', 1, 'orang'),
(3, 'Camping Plus Tenda', '1600000', 'asasa', 'Tenda Dome (ukuran 4 orang), Matras, Free Tiket Masuk, Free Pemasangan Tenda, Toilet Sharing', 1, 'orang (minimal 3 orang)'),
(4, 'Camping Rombongan STD', '260000', NULL, 'Camping Ground, Toliet, Listrik Luar Tenda', 2, 'pax / Night (min 50 pax)'),
(7, 'Camping Rombongan Outbond', '295000', NULL, 'Camping Ground, Toliet, Listrik Luar Tenda', 2, 'pax / Night (min 50 pax)'),
(8, 'Camping Rombongan Kasur', '395000', NULL, 'Camping Ground, Toliet, Listrik Luar Tenda', 2, 'pax / Night (min 50 pax)'),
(9, 'Camping Rombongan Outbound Kasur', '430000', NULL, 'Camping Ground, Toliet, Listrik Luar Tenda', 2, 'pax / Night (min 50 pax)'),
(10, 'Rombongan Besar', '240000', NULL, 'Minimal 50 Orang', 3, 'Orang'),
(11, 'Rombongan Kecil', '350000', NULL, 'Minimal 30 Orang', 3, 'Orang'),
(12, 'Itenerary', '150000', NULL, 'Tracking 1 - 1,5 jam', 4, 'Orang (Minimal 10 Orang)'),
(13, 'Bukit Batu Gede (Ground A)', '3700000', NULL, 'Dapat menampung 300 - 500 Orang dan memiliki 12 Toilet', 7, 'Malam'),
(14, 'Bukit Batu Hijau (Ground B)', '3500000', NULL, 'Dapat menampung 300 - 500 Orang dan memiliki 12 Toilet', 7, 'Malam'),
(15, 'Lembah Hijau (Ground C)', '2500000', NULL, 'Dapat menampung 200 - 300 Orang dan memiliki 10 Toilet', 7, 'Malam'),
(16, 'Lembah Pakis (Ground D)', '2500000', NULL, 'Dapat menampung 200 - 500 Orang dan memiliki 4 Toilet', 7, 'Malam'),
(17, 'Bukit Batu Pandan (Ground F)', '2500000', NULL, 'Dapat menampung 100 - 300 Orang dan memiliki 4 Toilet', 7, 'Malam'),
(18, 'Hari Kerja', '5000', NULL, '', 5, 'Orang'),
(19, 'Hari Kerja', '10000', NULL, '', 5, 'Mobil'),
(20, 'Hari Kerja', '5000', NULL, '', 5, 'Motor'),
(21, 'Hari Kerja', '100000', NULL, '', 5, 'Warga Negara Asing'),
(22, 'Hari Libur ', '7500', NULL, '', 5, 'Orang'),
(23, 'Hari Libur ', '15000', NULL, '', 5, 'Mobil'),
(24, 'Hari Libur', '7500', NULL, '', 5, 'Motor'),
(25, 'Hari Libur', '150000', NULL, '', 5, 'Warga Negara Asing');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telepon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_chart`
--
ALTER TABLE `tbl_chart`
  ADD PRIMARY KEY (`id_chart`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jadwal`
--
ALTER TABLE `tbl_jadwal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `tbl_kategori_produk`
--
ALTER TABLE `tbl_kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_konfirmasi`
--
ALTER TABLE `tbl_konfirmasi`
  ADD PRIMARY KEY (`id_konfirmasi`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_chart`
--
ALTER TABLE `tbl_chart`
  MODIFY `id_chart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_jadwal`
--
ALTER TABLE `tbl_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_kategori_produk`
--
ALTER TABLE `tbl_kategori_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_konfirmasi`
--
ALTER TABLE `tbl_konfirmasi`
  MODIFY `id_konfirmasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_jadwal`
--
ALTER TABLE `tbl_jadwal`
  ADD CONSTRAINT `tbl_jadwal_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `tbl_produk` (`id`);

--
-- Constraints for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD CONSTRAINT `id_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori_produk` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
