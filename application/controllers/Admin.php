<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_models');
		$this->load->model('user_models');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->helper('download');
		$this->load->library('upload');
	}

	public function doLogin()
	{
		if ($this->input->post()) {
			$login = $this->admin_models->getLogin();
			if ($login != '') {
				$data_session = array(
					'isLoginAdmin' => TRUE,
					'status' => "login",
					'username' => $login->username,
					'password' => $login->password
				);
				$this->session->set_userdata($data_session);
				redirect('admin/index');
			} else {
				$this->session->set_flashdata('msgError', 'Login failed, please enter your username and password.');
				redirect('admin/login');
				// echo 'Gagagl login';
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('admin/login'));
	}
	public function login()
	{
		// if ($this->session->userdata('isLoginAdmin') == FALSE) {
		//     redirect('admin/login');
		// }
		$this->load->view('admin/login');
	}

	public function index()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->load->view('admin/layout/header');
		$this->load->view('admin/index');
		$this->load->view('admin/layout/footer');
	}

	// Gallery
	public function gallery()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data = array(
			'galleries' => $this->admin_models->getGallery()
		);
		$this->load->view('admin/layout/header');
		$this->load->view('admin/gallery/gallery', $data);
		$this->load->view('admin/layout/footer');
	}

	public function addGallery()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->load->view('admin/layout/header');
		$this->load->view('admin/gallery/addGallery');
		$this->load->view('admin/layout/footer');
	}

	public function createGallery()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data = array(
			'title' => $this->input->post('title'),
			'description' => $this->input->post('description'),
			'image' => $this->uploadImage()
		);

		if ($this->input->post()) {
			$this->admin_models->addGallery($data, 'tbl_gallery');
			$this->session->set_flashdata('msgSuccess', 'Data Save Successfully!');
			redirect('/admin/gallery');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to save data');
			redirect('/admin/gallery');
		}
	}

	public function editGallery($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$where = array('id' => $id);
		$data['gallery'] = $this->admin_models->editGallery($where, 'tbl_gallery')->result();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/gallery/editGallery', $data);
		$this->load->view('admin/layout/footer');
	}

	public function updateGallery()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$description = $this->input->post('description');

		if (!empty($_FILES["image"]["name"])) {
			$image = $this->uploadImage();
		} else {
			$image =  $this->input->post["old_image"];
		}

		$data = array(
			'title' => $title,
			'description' => $description,
			'image' => $image
		);

		$where = array(
			'id' => $id
		);

		if ($this->input->post()) {
			$this->admin_models->updateGallery($where, $data, 'tbl_gallery');
			$this->session->set_flashdata('msgSuccess', 'Successfully update gallery.');
			redirect('/admin/gallery');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to update gallery!');
			redirect('/admin/gallery');
		}
	}

	public function deleteGallery($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->admin_models->deleteGallery($id, 'tbl_gallery');
		$this->session->set_flashdata('msgSuccess', 'Successfully delete Gallery.');
		redirect('/admin/gallery');
	}

	private function uploadImage()
	{
		$config['upload_path']		= './assets/images/';
		$config['allowed_types']	= 'gif|jpg|png|jpeg';
		$config['file_name']		= $this->id;
		$config['overwrite'] 		= true;
		$config['max_size'] 		= 3000;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		}

		return "default.png";
	}

	// Produk
	public function product()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['tbl_produk'] = $this->admin_models->getProduct();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/produk/produk', $data);
		$this->load->view('admin/layout/footer');
	}

	public function detailProduct($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->load->model('admin_models');
		$detail = $this->admin_models->detailProduct($id);
		$data['detail'] = $detail;

		$this->load->view('admin/layout/header');
		$this->load->view('admin/produk/detailProduk', $data);
		$this->load->view('admin/layout/footer');
	}

	public function addProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['kategori_produk'] = $this->admin_models->getKategori();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/produk/addProduk', $data);
		$this->load->view('admin/layout/footer');
	}

	public function createProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data = array(
			'nama_produk' => $this->input->post('nama_produk'),
			'deskripsi' => $this->input->post('deskripsi'),
			'price' => $this->input->post('price'),
			'des_price' => $this->input->post('des_price'),
			'id_kategori' => $this->input->post('id_kategori'),
			// 'image' => $this->input->post('image')
			// 'image' => $this->uploadImage()
		);

		if ($this->input->post()) {
			$this->admin_models->addProduct($data, 'tbl_produk');
			$this->session->set_flashdata('msgSuccess', 'Data Save Successfully!');
			redirect('/admin/product');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to save data');
			redirect('/admin/product');
		}
	}

	public function deleteProduct($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->admin_models->deleteProduct($id, 'tbl_produk');
		$this->session->set_flashdata('msgSuccess', 'Successfully delete Produk.');
		redirect('/admin/product');
	}

	public function editProduct($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$where = array('id' => $id);
		$data['kategori_produk'] = $this->admin_models->getKategori();
		$data['product'] = $this->admin_models->editProduct($where, 'tbl_produk')->result();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/produk/editProduk', $data);
		$this->load->view('admin/layout/footer');
		var_dump($data['product']);
	}

	public function updateProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$id = $this->input->post('id');
		$nama_produk = $this->input->post('nama_produk');
		$deskripsi = $this->input->post('deskripsi');
		$price = $this->input->post('price');
		$des_price = $this->input->post('des_price');
		$id_kategori = $this->input->post('id_kategori');


		$data = array(
			'nama_produk' => $nama_produk,
			'deskripsi' => $deskripsi,
			'price' => $price,
			'des_price' => $des_price,
			'id_kategori' => $id_kategori,
		);

		$where = array(
			'id' => $id
		);

		if ($this->input->post()) {
			$this->admin_models->updateProduct($where, $data, 'tbl_produk');
			$this->session->set_flashdata('msgSuccess', 'Successfully update produk.');
			redirect('/admin/product');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to update produk!');
			redirect('/admin/product');
		}
	}

	// Kategori Produk
	public function CategoryProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['categoryProducts'] = $this->admin_models->getKategori();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/kategori-produk/kategoriProduk', $data);
		$this->load->view('admin/layout/footer');
	}

	public function addCategoryProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['kategori_produk'] = $this->admin_models->getKategori();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/kategori-produk/addKategoriProduk', $data);
		$this->load->view('admin/layout/footer');
	}

	public function createCategoryProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data = array(
			'nama_kategori' => $this->input->post('nama_kategori'),
			'deskripsi' => $this->input->post('deskripsi'),
			'biaya_tambahan' => $this->input->post('biaya_tambahan'),
			'image' => $this->uploadImage()
		);

		if ($this->input->post()) {
			$this->admin_models->addCategoryProduct($data, 'tbl_kategori_produk');
			$this->session->set_flashdata('msgSuccess', 'Data Save Successfully!');
			redirect('/admin/CategoryProduct');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to save data');
			redirect('/admin/CategoryProduct');
		}
	}

	public function deleteCategoryProduct($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->admin_models->deleteCategoryProduct($id, 'tbl_kategori_produk');
		$this->session->set_flashdata('msgSuccess', 'Successfully delete Produk.');
		redirect('/admin/CategoryProduct');
	}

	public function editCategoryProduct($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$where = array('id' => $id);
		$data['CategoryProduct'] = $this->admin_models->editCategoryProduct($where, 'tbl_kategori_produk')->result();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/kategori-produk/editKategoriProduk', $data);
		$this->load->view('admin/layout/footer');
	}

	public function updateCategoryProduct()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$id = $this->input->post('id');
		$nama_kategori = $this->input->post('nama_kategori');
		$deskripsi = $this->input->post('deskripsi');
		$biaya_tambahan = $this->input->post('biaya_tambahan');

		if (!empty($_FILES["image"]["name"])) {
			$image = $this->uploadImage();
		} else {
			$image =  $this->input->post["old_image"];
		}


		$data = array(
			'nama_kategori' => $nama_kategori,
			'deskripsi' => $deskripsi,
			'biaya_tambahan' => $biaya_tambahan,
			'image' => $image
		);

		$where = array(
			'id' => $id
		);

		if ($this->input->post()) {
			$this->admin_models->updateCategoryProduct($where, $data, 'tbl_kategori_produk');
			$this->session->set_flashdata('msgSuccess', 'Successfully update produk.');
			redirect('/admin/CategoryProduct');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to update produk!');
			redirect('/admin/CategoryProduct');
		}
	}

	public function detailCategoryProduct($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->load->model('admin_models');
		$detail = $this->admin_models->detailCategoryProduct($id);
		$data['detail'] = $detail;
		$this->load->view('admin/layout/header');
		$this->load->view('admin/kategori-produk/detailKategoriProduk', $data);
		$this->load->view('admin/layout/footer');
	}

	// News
	public function News()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['news'] = $this->admin_models->getNews();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/news/news', $data);
		$this->load->view('admin/layout/footer');
	}

	public function addNews()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['news'] = $this->admin_models->getKategori();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/news/addNews', $data);
		$this->load->view('admin/layout/footer');
	}

	public function createNews()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data = array(
			'title' => $this->input->post('title'),
			'deskripsi' => $this->input->post('deskripsi'),
			'image' => $this->uploadImage()
		);

		if ($this->input->post()) {
			$this->admin_models->addNews($data, 'tbl_news');
			$this->session->set_flashdata('msgSuccess', 'Data Save Successfully!');
			redirect('/admin/News');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to save data');
			redirect('/admin/News');
		}
	}

	public function deleteNews($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->admin_models->deleteNews($id, 'tbl_news');
		$this->session->set_flashdata('msgSuccess', 'Successfully delete news.');
		redirect('/admin/News');
	}

	public function editNews($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$where = array('id' => $id);
		$data['news'] = $this->admin_models->editNews($where, 'tbl_news')->result();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/news/editNews', $data);
		$this->load->view('admin/layout/footer');
	}

	public function updateNews()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$id = $this->input->post('id');
		$title = $this->input->post('title');
		$deskripsi = $this->input->post('deskripsi');

		if (!empty($_FILES["image"]["name"])) {
			$image = $this->uploadImage();
		} else {
			$image =  $this->input->post["old_image"];
		}

		$data = array(
			'title' => $title,
			'deskripsi' => $deskripsi,
			'image' => $image
		);

		$where = array(
			'id' => $id
		);

		if ($this->input->post()) {
			$this->admin_models->updateNews($where, $data, 'tbl_news');
			$this->session->set_flashdata('msgSuccess', 'Successfully update news.');
			redirect('/admin/News');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to update news!');
			redirect('/admin/News');
		}
	}

	public function detailNews($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->load->model('admin_models');
		$detail = $this->admin_models->detailNews($id);
		$data['detail'] = $detail;
		$this->load->view('admin/layout/header');
		$this->load->view('admin/news/detailNews', $data);
		$this->load->view('admin/layout/footer');
	}

	// Jadwal
	public function Schedule()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['tbl_jadwal'] = $this->admin_models->getSchedule();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/jadwal/jadwal', $data);
		$this->load->view('admin/layout/footer');
	}

	public function detailSchedule($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->load->model('admin_models');
		$detail = $this->admin_models->detailSchedule($id);
		$data['detail'] = $detail;

		$this->load->view('admin/layout/header');
		$this->load->view('admin/jadwal/detailJadwal', $data);
		$this->load->view('admin/layout/footer');
	}

	public function addSchedule()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data['produk'] = $this->admin_models->getProduct();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/jadwal/addJadwal', $data);
		$this->load->view('admin/layout/footer');
	}

	public function createSchedule()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$data = array(
			'tanggal' => $this->input->post('tanggal'),
			'id_produk' => $this->input->post('id_produk'),
		);

		if ($this->input->post()) {
			$this->admin_models->addSchedule($data, 'tbl_jadwal');
			$this->session->set_flashdata('msgSuccess', 'Data Save Successfully!');
			redirect('/admin/Schedule');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to save data');
			redirect('/admin/Schedule');
		}
	}

	public function deleteSchedule($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$this->admin_models->deleteSchedule($id, 'tbl_jadwal');
		$this->session->set_flashdata('msgSuccess', 'Successfully delete Produk.');
		redirect('/admin/Schedule');
	}

	public function editSchedule($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$where = array('id' => $id);
		$data['produk'] = $this->admin_models->getProduct();
		$data['Schedule'] = $this->admin_models->editSchedule($where, 'tbl_jadwal')->result();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/jadwal/editJadwal', $data);
		$this->load->view('admin/layout/footer');
		var_dump($data['Schedule']);
	}

	public function updateSchedule()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('admin/login');
		}

		$id = $this->input->post('id');
		$tanggal = $this->input->post('tanggal');
		$id_produk = $this->input->post('id_produk');

		$data = array(
			'tanggal' => $tanggal,
			'id_produk' => $id_produk,
		);

		$where = array(
			'id' => $id
		);

		if ($this->input->post()) {
			$this->admin_models->updateSchedule($where, $data, 'tbl_jadwal');
			$this->session->set_flashdata('msgSuccess', 'Successfully update produk.');
			redirect('/admin/Schedule');
		} else {
			$this->session->set_flashdata('msgError', 'Failed to update produk!');
			redirect('/admin/Schedule');
		}
	}

	public function Transaksi()
	{
		$data['transaksi'] = $this->admin_models->getAllChart();
		$this->load->view('admin/layout/header');
		$this->load->view('admin/transaksi/transaksi', $data);
		$this->load->view('admin/layout/footer');
	}

	public function detailTransaksi($idChart)
	{
		$data['transaksi'] = $this->admin_models->getChartById($idChart);
		$this->load->view('admin/layout/header');
		$this->load->view('admin/transaksi/detailTransaksi', $data);
		$this->load->view('admin/layout/footer');
	}

	public function konfirmasiByAdmin()
	{
		if ($this->input->post()) {
			$dataChart = array(
				'id_chart' => $this->input->post('id_chart'),
				'id_user' => $this->input->post('id_user'),
				'id_produk' => $this->input->post('id_produk'),
				'id_tanggal' => $this->input->post('id_tanggal'),
				'qty' => $this->input->post('qty'),
				'status' => 'KONFIRMASI_BY_ADMIN'
			);
			$this->user_models->editChart($this->input->post('id_chart'), $dataChart);
			// $this->autoResponderEmailRegister($dataChart['email']);

			redirect('/admin/transaksi');
		} else { 
			redirect('/admin/transaksi');
		}
	}

	public function tolakByAdmin()
	{
		if ($this->input->post()) {
			$dataChart = array(
				'id_chart' => $this->input->post('id_chart'),
				'id_user' => $this->input->post('id_user'),
				'id_produk' => $this->input->post('id_produk'),
				'id_tanggal' => $this->input->post('id_tanggal'),
				'qty' => $this->input->post('qty'),
				'status' => 'ORDER_DITOLAK'
			);
			$this->user_models->editChart($this->input->post('id_chart'), $dataChart);
			redirect('/admin/transaksi');
		} else { 
			redirect('/admin/transaksi');
		}
	}

	public function autoResponderEmailRegister($email)
    {
        $this->load->config('email');
        $this->load->library('email');

        $from = $this->config->item('smtp_user');

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($email);
        $this->email->subject('Pendaftaran Berhasil');
        $body = $this->load->view('/v2/user/layout/email-register-business.php', '', true);
        $this->email->message('test');

        if ($this->email->send()) {
            return true;
        }
    }
}
