<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_models');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
	}

	public function index()
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$this->load->view('user/index');
		$this->load->view('user/layout/footer');
	}

	public function contactUs()
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$this->load->view('user/contactus');
		$this->load->view('user/layout/footer');
	}

	public function gallery()
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$data = array(
			'galleries' => $this->user_models->getGallery()
		);
		$this->load->view('user/gallery', $data);
		$this->load->view('user/layout/footer');
	}

	public function news()
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$data['news'] = $this->user_models->getNews();
		$this->load->view('user/news', $data);
		$this->load->view('user/layout/footer');
	}

	public function newsDetail($id)
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$this->load->model('user_models');
		$detail = $this->user_models->detailNews($id);
		$data['detail'] = $detail;
		$data['news'] = $this->user_models->getNews();
		$this->load->view('user/newsDetail1', $data);
		$this->load->view('user/layout/footer');
	}

	public function package()
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$data['super_product'] = $this->user_models->getSuperProduct();
		$this->load->view('user/package', $data);
		$this->load->view('user/layout/footer');
	}

	public function detailPackage($id)
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$data['detail'] = $this->user_models->getDetailPackage2($id);
		$data['produk'] = $this->user_models->getDetailPackage($id);
		// $email = $this->session->userdata('email');
		// $idUser = $this->user_models->getUserData($email);
		$this->load->view('user/detailPackage', $data);
		$this->load->view('user/layout/footer');
		// var_dump($idUser->id);

	}

	public function detailSuperPackage($id)
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$data['detail'] = $this->user_models->getSuperPackage2($id);
		$data['produk'] = $this->user_models->getSuperPackage($id);
		$this->load->view('user/detailSuperPackage', $data);
		$this->load->view('user/layout/footer');
		// var_dump($data['produk']);

	}

	public function login()
	{
		$this->load->view('user/layout/header');
		$this->load->view('user/login');
		$this->load->view('user/layout/footer');
	}

	public function doLogin()
	{
		if ($this->input->post()) {
			$login = $this->user_models->getLogin();
			if ($login != '') {
				$data_session = array(
					'status' => "login",
					'email' => $this->input->post('email'),
					'password' => md5($this->input->post('password'))
				);
				$this->session->set_userdata($data_session);
				redirect('user/profile');
			} else {
				$this->session->set_flashdata('msgError', 'Login failed, please enter your username and password.');
				redirect('user/login');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('user/login'));
	}

	public function register()
	{
		$this->load->view('user/layout/header');
		$this->load->view('user/register');
		$this->load->view('user/layout/footer');
	}

	public function order()
	{
		if ($this->session->userdata('status') == "login") {
			$this->load->view('user/layout/headerLogin');
		} else {
			$this->load->view('user/layout/header');
		}

		$this->load->view('user/order');
		$this->load->view('user/layout/footer');
	}

	// Register User CRUD
	public function registerUser()
	{
		if ($this->input->post()) {
			$data = array(
				'nama' => $this->input->post('nama'),
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'telepon' => $this->input->post('telepon'),
			);
			$this->user_models->addUser($data);
			redirect('/login');
		} else {
			$this->session->set_flashdata('Error', 'Gagal registrasi, Silakan Ulangi');
		}
	}

	public function profile()
	{
		$email = $this->session->userdata('email');
		$data['userData'] = $this->user_models->getUserData($email);
		$this->load->view('user/layout/headerLogin');
		$this->load->view('user/profile', $data);
		$this->load->view('user/layout/footer');
		// echo '<pre>';
		// var_dump($data['userData']);
		// echo '</pre>';
	}

	public function historyTransaction()
	{
		if ($this->session->userdata('status') != "login") {
			redirect('login');
		}
		$email = $this->session->userdata('email');
		$userData = $this->user_models->getUserData($email);
		$idUser = $userData->id;
		$data['chart'] = $this->user_models->getChart($idUser);
		// $data['chart_detail'] = $this->user_models->getChartById();
		$this->load->view('user/layout/headerLogin');
		$this->load->view('user/historyTransaction', $data);
		$this->load->view('user/layout/footer');
	}

	public function historyTransactionDetail($id)
	{
		if ($this->session->userdata('status') != "login") {
			redirect('login');
		}
		$data['id_chart'] = $id;
		$data['chart_detail'] = $this->user_models->getChartById($id);
		$this->load->view('user/layout/headerLogin');
		$this->load->view('user/historyKonfirmasi', $data);
		$this->load->view('user/layout/footer');
	}

	function konfirmasiUser()
	{
		if ($this->input->post()) {
			$config['upload_path']          = './assets/img/konfirmasi';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['max_size']             = 10000;
			$config['max_width']            = 10000;
			$config['max_height']           = 10000;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')) {
				$error = array('error' => $this->upload->display_errors());
				echo '<pre>';
				var_dump($error);
				echo '</pre>';
				// redirect('user/historyTransaction');
			} else {
				$data = array(
					'id_chart' => $this->input->post('id_chart'),
					'deskripsi' => $this->input->post('deskripsi'),
					'image' => $this->upload->data('file_name'),
				);
				$this->user_models->userKonfimasi($data);
				$email = $this->session->userdata('email');
				$userData = $this->user_models->getUserData($email);
				$idUser = $userData->id;
				$idChart = $this->input->post('id_chart');
				$dataChart = array(
					'id_chart' => $idChart,
					'id_user' => $idUser,
					'id_produk' => $this->input->post('id_produk'),
					'id_tanggal' => $this->input->post('id_tanggal'),
					'qty' => $this->input->post('qty'),
					'status' => 'USER_KONFIRMASI'
				);
				$this->user_models->editChart($idChart, $dataChart);
				redirect('user/historyTransaction');
			}
		} else {
			// Else in here
		}
	}

	public function postCheckout()
	{
		$email = $this->session->userdata('email');
		$idUser = $this->user_models->getUserData($email);
		if ($this->input->post()) {
			$data = array(
				'id_user' => $idUser->id,
				'id_produk' => $this->input->post('id_produk'),
				'id_tanggal' => $this->input->post('id_tanggal'),
				'qty' => $this->input->post('qty'),
				'status' => 'MENUNGGU_PEMBAYARAN'
			);
			$this->user_models->addToChart($data);
			redirect('/history-transaksi');
		} else {
			$this->session->set_flashdata('Error', 'Gagal Menambahkan ke keranjang');
		}
	}

	public function updateKonfirmasiChart()
	{
		$email = $this->session->userdata('email');
		$userData = $this->user_models->getUserData($email);
		$idUser = $userData->id;
		$idChart = 4;
		if ($this->input->post()) {
			$data = array(
				'id_chart' => $idChart,
				'id_user' => $idUser,
				'id_produk' => 7,
				'id_tanggal' => 18,
				'qty' => 10,
				'status' => 'USER_KONFIRMASI'
			);
			$this->user_models->editChart($idChart, $data);
			redirect('/history-transaksi');
		} else {
			$this->session->set_flashdata('Error', 'Gagal Konfirmasi');
		}
	}
}
