<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_models extends CI_Model
{
	function getLogin()
	{
		$this->db->where('email', $this->input->post('email'));
		$this->db->where('password', md5($this->input->post('password')));
		$query = $this->db->get('tbl_user');
		if ($query->num_rows() == 1)
			return $query->row();
		else
			return '';
	}

	// Gallery
	public function getGallery()
	{
		$sql = $this->db->get('tbl_gallery');
		return $sql->result();
	}

	// News
	public function getNews()
	{
		$sql = $this->db->get('tbl_news');
		return $sql->result();
	}

	public function detailNews($id)
	{
		$sql = $this->db->query("SELECT `id`, `title`, `deskripsi`, `image`, `createdAt`, `updatedAt` FROM `tbl_news` WHERE id = $id");
		return $sql->row();
	}

	public function registerMember($data)
	{
		$this->db->insert('tbl_user', $data);
	}

	// Produk
	public function getSuperProduct()
	{
		$sql = $this->db->get('tbl_kategori_produk');
		return $sql->result();
	}

	public function getProduct()
	{
		$sql = $this->db->query("SELECT
			a.id,
			a.nama_produk,
			a.price,
			a.deskripsi,
			a.image,
			a.id_kategori,
			a.des_price,
			b.nama_kategori
		FROM
			tbl_produk a 
			LEFT JOIN tbl_kategori_produk b 
			ON a.id_kategori = b.id");
		return $sql->result();
	}

	public function getDetailPackage($id)
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.nama_produk,
		b.price,
		b.image,
		b.deskripsi,
		b.id_kategori,
		b.des_price
	FROM
		 tbl_jadwal a
		 LEFT JOIN tbl_produk b 
		 ON b.id = a.id_produk
	WHERE
		a.id_produk = $id");
		return $sql->result();
	}

	public function getDetailPackage2($id)
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.nama_produk,
		b.price,
		b.image,
		b.deskripsi,
		b.id_kategori,
		b.des_price,
		c.nama_kategori
	FROM
		 tbl_jadwal a
		 LEFT JOIN tbl_produk b 
		 ON b.id = a.id_produk
		 LEFT JOIN tbl_kategori_produk c
		 ON c.id = b.id_kategori
	WHERE
		a.id_produk = $id");
		return $sql->row();
	}

	public function getSuperPackage($id)
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.id,
		b.nama_produk,
		b.price,
		b.image,
		b.deskripsi,
		b.id_kategori,
		b.des_price
	FROM
		 tbl_kategori_produk a
		 LEFT JOIN tbl_produk b 
		 ON b.id_kategori = a.id
	WHERE
		a.id = $id");
		return $sql->result();
	}

	public function getSuperPackage2($id)
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.id,
		b.nama_produk,
		b.price,
		b.image,
		b.deskripsi,
		b.id_kategori,
		b.des_price
	FROM
		tbl_kategori_produk a
		 LEFT JOIN tbl_produk b 
		 ON b.id_kategori = a.id
	WHERE
		a.id = $id");
		return $sql->row();
	}

	// CRUD Register USer
	public function addUser($data)
	{
		$this->db->insert('tbl_user', $data);
	}
	public function getUserData($email)
	{
		$sql = $this->db->query("SELECT * FROM `tbl_user` WHERE email = '$email' ");
		return $sql->row();
	}
	public function addToChart($data)
	{
		$this->db->insert('tbl_chart', $data);
	}
	public function getChart($idUser)
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.nama_produk,
		b.price * a.qty AS total
	FROM
		tbl_chart a
		LEFT JOIN tbl_produk b 
		ON b.id = a.id_produk
	WHERE
		a.id_user = $idUser ");
		return $sql->result();
	}
	public function userKonfimasi($data)
	{
		$this->db->insert('tbl_konfirmasi', $data);
	}
	public function getChartById($idChart)
	{
		$sql = $this->db->query("SELECT 
		a.*,
		b.nama_produk,
		b.price * a.qty AS total
	FROM 
		tbl_chart a
		LEFT JOIN tbl_produk b 
		ON b.id = a.id_produk 
	WHERE 
		id_chart = $idChart ");
		return $sql->row();
	}
	public function editChart($idChart, $data)
	{
		$this->db->where('id_chart', $idChart);
		$this->db->update('tbl_chart', $data);
	}
}
