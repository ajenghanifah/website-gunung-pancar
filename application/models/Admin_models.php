<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_models extends CI_Model
{
	function getLogin()
	{
		$this->db->where('username', $this->input->post('username'));
		$this->db->where('password', $this->input->post('password'));
		$query = $this->db->get('tbl_admin');
		if ($query->num_rows() == 1)
			return $query->row();
		else
			return '';
	}

	// Gallery
	public function getGallery()
	{
		$sql = $this->db->get('tbl_gallery');
		return $sql->result();
	}

	public function addGallery($data)
	{
		$this->db->insert('tbl_gallery', $data);
	}

	function deleteGallery($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_gallery');
	}

	function updateGallery($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function editGallery($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	// Produk
	public function getProduct()
	{
		$sql = $this->db->query("SELECT
			a.id,
			a.nama_produk,
			a.price,
			a.deskripsi,
			a.image,
			a.id_kategori,
			a.des_price,
			b.nama_kategori
		FROM
			tbl_produk a 
			LEFT JOIN tbl_kategori_produk b 
			ON a.id_kategori = b.id");
		return $sql->result();
	}

	public function addProduct($data)
	{
		$this->db->insert('tbl_produk', $data);
	}

	function deleteProduct($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_produk');
	}

	function updateProduct($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function editProduct($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	function detailProduct($id)
	{
		$sql = $this->db->query("SELECT
		a.id,
		b.nama_kategori,
		a.nama_produk,
		a.price,
		a.image,
		a.deskripsi,
		a.id_kategori,
		a.des_price
	FROM
		tbl_produk a
	LEFT JOIN tbl_kategori_produk b ON
		a.id_kategori = b.id
	WHERE
		a.id = $id");
		return $sql->row();
	}

	// Kategori Produk
	public function getKategori()
	{
		$sql = $this->db->get('tbl_kategori_produk');
		return $sql->result();
	}

	public function addCategoryProduct($data)
	{
		$this->db->insert('tbl_kategori_produk', $data);
	}

	function deleteCategoryProduct($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_kategori_produk');
	}

	function updateCategoryProduct($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function editCategoryProduct($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	function detailCategoryProduct($id)
	{
		$sql = $this->db->query("SELECT `id`, `nama_kategori`, `deskripsi`, `biaya_tambahan` FROM `tbl_kategori_produk` WHERE id = $id");
		return $sql->row();
	}

	// News
	public function getNews()
	{
		$sql = $this->db->get('tbl_news');
		return $sql;
	}

	public function addNews($data)
	{
		$this->db->insert('tbl_news', $data);
	}

	function deleteNews($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_news');
	}

	function updateNews($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function editNews($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	public function detailNews($id)
	{
		$sql = $this->db->query("SELECT `id`, `title`, `deskripsi`, `image`, `createdAt`, `updatedAt` FROM `tbl_news` WHERE id = $id");
		return $sql->row();
	}

	// Jadwal Paket / Tiket
	public function getSchedule()
	{
		$sql = $this->db->query("SELECT
		a.id,
		a.tanggal,
		a.id_produk,
		b.nama_produk
	FROM
		tbl_jadwal a 
		LEFT JOIN tbl_produk b 
		ON a.id_produk = b.id");
		return $sql->result();
	}

	public function addSchedule($data)
	{
		$this->db->insert('tbl_jadwal', $data);
	}

	function deleteSchedule($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_jadwal');
	}

	function updateSchedule($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	function editSchedule($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	function detailSchedule($id)
	{
		$sql = $this->db->query("SELECT `id`, `tanggal`, `id_produk` FROM `tbl_jadwal` WHERE id = $id");
		return $sql->row();
	}
	public function getAllChart()
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.nama_produk,
		b.price * a.qty AS total,
		c.nama AS nama_pelanggan,
		d.image
	FROM
		tbl_chart a
	LEFT JOIN tbl_produk b ON
		b.id = a.id_produk
	LEFT JOIN tbl_user c ON
		c.id = a.id_user
		LEFT JOIN tbl_konfirmasi d ON
		d.id_chart = a.id_chart");
		return $sql->result();
	}
	public function getChartById($idChart)
	{
		$sql = $this->db->query("SELECT
		a.*,
		b.nama_produk,
		b.price * a.qty AS total,
		c.nama AS nama_pelanggan,
		c.email,
		c.telepon,
		d.image,
		e.tanggal
	FROM
		tbl_chart a
	LEFT JOIN tbl_produk b ON
		b.id = a.id_produk
	LEFT JOIN tbl_user c ON
		c.id = a.id_user
		LEFT JOIN tbl_konfirmasi d ON
		d.id_chart = a.id_chart
		LEFT JOIN tbl_jadwal e ON
		e.id = a.id_tanggal
	WHERE a.id_chart = $idChart");
		return $sql->row();
	}
}
