<html>
<?php $this->load->view('user/layout/style.php') ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light gp-header">
    <div class="container">
        <a class="navbar-brand" href="<?php echo base_url() ?>">
            <img class="logo-navbar" src="<?php echo base_url('assets/user/') ?>images/logo-navbar.png" class="img-fluid" alt="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url() ?>">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('paket') ?>">Biaya Paket dan Tiket Masuk</a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('berita') ?>">Berita</a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('gallery') ?>">Gallery</a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('contact') ?>">Hubungi Kami</a></li>
            </ul>
        </div>
    </div>
</nav>

</html>