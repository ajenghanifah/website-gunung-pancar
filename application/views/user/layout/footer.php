<footer class="page-footer font-small blue" style="font-size: 12px;
   left: 0;
   bottom: 0;
   width: 100%; ">
    <div class="container-fluid text-center text-md-left" style="background-color: #1d5e1f; color: white; padding: 30px;">
        <div class="row">
            <div class="col-md-3 mb-md-0 mb-3">
                <h6 class="text-uppercase">Info Lebih Lanjut</h6>
                <ul class="list-unstyled">
                    <li>
                        <p>Home <br>
                            Paket <br>
                            Biaya dan Tiket Masuk
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h6 class="text-uppercase">Cerita Kami</h6>
                <ul class="list-unstyled">
                    <li>
                        <p>Berita <br>
                            Galery
                        </p>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h6 class="text-uppercase">Hubungi Kami</h6>
                <ul class="list-unstyled">
                    <li>
                        <p><strong>TWA Gunung Pancar</strong> <br>
                            KP Cibural, Desa Karang Tengah
                            Kec. Babakan Madang, Kab. Bogor
                            Jawa Barat, 16810</p>
                    </li>
                    <li>
                        <p>Contact Person ( 08.00 - 19.00 WIB )<br>
                            Informasi: <br>
                            <i class="fa fa-phone" aria-hidden="true"></i></i> 081213446514 <br>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> info@gunungpancar.com
                        </p>
                    </li>
                    <li>
                        <p class="margin-bottom-0">Event Organizer dan Program: <br>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> gunungpancarorginizer@gmail.com
                        </p>
                    </li>
                </ul>

            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <h6 class="text-uppercase">Follow Us</h6>
                <ul class="list-unstyled">
                    <li>
                        <i class="fa fa-facebook fa-2x"></i>
                        <i class="fa fa-instagram fa-2x" style="margin: 10px 20px;"></i>
                        <i class="fa fa-twitter fa-2x"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3" style="background-color: #85b237;">© 2020 Copyright - Romy Ardiyanto</div>
</footer>