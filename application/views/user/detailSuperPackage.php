<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="package">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="package-title">
                    <p>BIAYA PAKET DAN TIKET MASUK</p>
                    <div class="underline-package"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="pricing py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3><?php echo $detail->nama_kategori; ?></h3>
                <div class="underline-package-title"></div>
            </div>
            <?php
            foreach ($produk as $item) {
                ?>
                <div class="col-lg-4">
                    <a href="<?php echo base_url() ?>User/detailPackage/<?php echo $item->id; ?>" style="text-decoration: none; color:balck">
                        <div class="card" style="margin-bottom: 20px;">
                            <div class="card-body">
                                <h5 class="card-title text-muted text-uppercase text-center"><?php echo substr($item->nama_produk, 0, 35); ?></h5>
                                <h6 class="card-price text-center">Rp. <?php echo number_format($item->price); ?><span class="period"><br>/ <?php echo substr($item->des_price, 0, 35); ?></span></h6>
                                <hr>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fa fa-check" aria-hidden="true"></i></span><?php echo substr($item->deskripsi, 0, 35); ?></li>
                                </ul>
                                <!-- <div class="text-center sn-business-opportunity-button-register">
                                    <a href="<?php echo base_url('order') ?>" class="">Beli Paket</a>
                                </div> -->
                            </div>
                        </div>
                    </a>
                </div>
            <?php
            } ?>

        </div>
    </div>
</section>

<div class="col-lg-12 text-center">
    <div class="sn-business-opportunity-register ">
        <p>Silakan hubungi kami terlebih dahulu untuk jadwal survey dan reservasi</p>
        <a href="<?php echo base_url('contact') ?>">Klik disini!</a>
    </div>
    <!-- <div class="sn-business-opportunity-background-register">
        <div>
            <p>Beli Paket Sekarang!</p>
        </div>
        <div class="sn-business-opportunity-button-register">
            <a href="<?php echo base_url('login') ?>">Beli Paket</a>
        </div>
    </div> -->
</div>

</html>