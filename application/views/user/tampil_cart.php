<html>
<?php $this->load->view('user/layout/style.php') ?>


<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light gp-header">
    <div class="container">
        <a class="navbar-brand" href="<?php echo base_url() ?>">
            <img class="logo-navbar" src="<?php echo base_url('assets/user/') ?>images/logo-navbar.png" class="img-fluid" alt="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url() ?>">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-item"><a class="text-header nav-link text-nav-active active" href="<?php echo base_url('paket') ?>">Biaya Paket dan Tiket Masuk</a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('berita') ?>">Berita</a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('gallery') ?>">Gallery</a></li>
                <li class="nav-item"><a class="text-header nav-link" href="<?php echo base_url('contact') ?>">Hubungi Kami</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="package">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="package-title">
                    <p>BIAYA PAKET DAN TIKET MASUK <?php echo $this->session->userdata('password') ?></p>
                    <div class="underline-package"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <section class="pricing py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Paket Camping</h3>
                <div class="underline-package-title"></div>
            </div>
            <?php
            foreach ($tbl_produk as $item) {
                ?>
                <div class="col-lg-4">
                    <div class="card mb-5 mb-lg-0">
                        <div class="card-body">
                            <h5 class="card-title text-muted text-uppercase text-center"><?php echo substr($item->nama_produk, 0, 35); ?></h5>
                            <h6 class="card-price text-center">Rp. <?php echo substr($item->price, 0, 35); ?><span class="period">/ <?php echo substr($item->des_price, 0, 35); ?></span></h6>
                            <hr>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fa fa-check" aria-hidden="true"></i></span><?php echo substr($item->deskripsi, 0, 35); ?></li>
                            </ul>
                            <div class="text-center sn-business-opportunity-button-register">
                                <a href="#" class="">Beli Paket</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } ?>

        </div>
    </div>
</section> -->

<div class="col-lg-12 text-center">
    <div class="sn-business-opportunity-register ">
        <p>Silakan hubungi kami terlebih dahulu untuk jadwal survey dan reservasi</p>
        <a href="<?php echo base_url('contact') ?>">Klik disini!</a>
    </div>
</div>

</html>