<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="banner-home">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100 carousel-image-small" src="<?php echo base_url('assets/user/') ?>images/carousel-1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 carousel-image-small" src="<?php echo base_url('assets/user/') ?>images/carousel-2.jpg" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 carousel-image-small" src="<?php echo base_url('assets/user/') ?>images/carousel-3.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="margin-top-40">TAMAN WISATA ALAM GUNUNG PANCAR</h2>
        </div>
        <div class="col-lg-8">
            <h4 class="margin-top-40">Sekilas Gunung Pancar</h4>
            <p class="text-home">Gunung Pancar terletak di desa Karang Tengah, Babakan Madang, Bogor, Jawa Barat. Tapi lokasi ini juga fimiliar
                dengan sebutan Sentul. Karena lokasi Gunung Pancar sangan dekat dengan Sentul City. Dengan ketinggian 300 - 800 mdpl,
                maka suhu di Gunung Pancar tidak terlalu berbeda dengan suhu di kota, terutamaSentul yang paling berdekatan dengan lokasi.
                Mayoritas vegetasi yang ada di Gunung Pancar adalah Pinus, atau bisa kita sebut hutan Pinus. Namun di lokasi ini juga
                terdapat bermacam vegetasi yang hidup dan tumbuh, yaitu Rasmala, Puspa, Pasang, Beringin dan lainnya.
            </p>
            <p class="text-home">Gunung Pancar ditempuh dengan berkendara via toll Jagorawi dari Jakarta. Lokasi Camping Ground ini dekat dengan Jakarta.
                Anda cukup menempuh perjalanan 1 - 1,5 jam dengan keadaan lalu lintas normal. Kemudia anda keluar di pintu toll Sentul Selatan /
                Sentul City (Setelah pintu toll Circuit Sentul dari arah Jakarta). Dari Sentul, anda cukup mengikuti arah petunjuk Jungle,
                karena Gunung Pancar dan Jungle Land Sentul sangat berdekatan.
            </p>
            <h4>Camping Ground Gunung Pancar Bogor</h4>
            <p class="text-home">Gunung Pancar sering dipakai untuk kegiatan Outbound Team Building, Camping, Gathering, Pelatihan Kepemimpinan, dan
                kegiatan outdoor lainnya. Jika anda mencari tempat camping di Bogor yang tidak jauh dari Jakarta dan ramah akan keluarga,
                maka Gunung Pancar adalah salah satu pilihan terbaik. Lokasi ini juga sering dipakai para calon pasangan pengantin untuk mengambil
                gambar pranikah atau prewedding. Hutan Pinus Sentul, negitulah para wisatawan biasa menyebutnya. Anak-anak sekolah juga sering mengadakan
                camping atau kemah disini. Selain aman, Hutan Pinus Gunung Pancar Sentul ini juga menyediakan fasilitas dan paket-paket yang
                lengkap. Salah satu camping ground terbaik di Bogor ini dirawat dengan baik oleh para staff kebersihan, sehingga keasrian dan kenyamanan anda
                dalan melakukan kegiaatan outbounf team building, camping dan lainnya tetap terjaga.
            </p>
            <p class="text-home">Bagi anda yang menginginkan paket-paket lengkap dan murah terjangkau, silakan kunjungi paket-paket yang telah kamu
                rancang untuk Anda dan hubungi kami.
            </p>
        </div>
        <div class="col-lg-4">
            <div class="gallery-column margin-top-40">
                <a href="<?php echo base_url('assets/user/') ?>images/home-1.jpg" class="fancybox" rel="ligthbox">
                    <img class="zooms img-fluid gallery-image" src="<?php echo base_url('assets/user/') ?>images/home-1.jpg" alt="">
                </a>
            </div>
            <div class="gallery-column margin-top-40">
                <a href="<?php echo base_url('assets/user/') ?>images/home-2.jpg" class="fancybox" rel="ligthbox">
                    <img class="zooms img-fluid gallery-image" src="<?php echo base_url('assets/user/') ?>images/home-2.jpg" alt="">
                </a>
            </div>
            <div class="gallery-column margin-top-40">
                <a href="<?php echo base_url('assets/user/') ?>images/home-3.jpg" class="fancybox" rel="ligthbox">
                    <img class="zooms img-fluid gallery-image" src="<?php echo base_url('assets/user/') ?>images/home-3.jpg" alt="">
                </a>
            </div>
            <div class="gallery-column margin-top-40 margin-bottom-40">
                <a href="<?php echo base_url('assets/user/') ?>images/home-4.jpg" class="fancybox" rel="ligthbox">
                    <img class="zooms img-fluid gallery-image" src="<?php echo base_url('assets/user/') ?>images/home-4.jpg" alt="">
                </a>
            </div>
        </div>
    </div>
</div>

</html>