<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="news">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="news-content">
                    <?php
                    foreach ($news as $item) {
                    ?>
                        <div>
                            <img class="news-image" src="<?php echo base_url('assets/images/' . $item->image) ?>" alt="">
                            <div class="news-content-title">
                                <p><?php echo $item->title; ?></p>
                            </div>
                            <div class="news-content-date text-muted">
                                <p><?php echo $item->createdAt; ?> WIB</p>
                            </div>
                            <div class="news-content-paragraph">
                                <p><?php echo substr($item->deskripsi, 0, 400); ?>...</p>
                            </div>
                            <div class="button-read-more">
                                <a href="<?php echo base_url() ?>User/newsDetail/<?php echo $item->id; ?>">BACA
                                    SELENGKAPNYA</a>
                            </div>
                        </div>
                    <?php
                    } ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="news-search">
                    <div class="news-search-bar">
                        <input class="news-search-input" type="text" name="" placeholder="Search...">
                        <a href="#" class="">
                            <img class="news-search-icon" src="<?php echo base_url('assets/user/') ?>images/search-icon.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="news-update">
                    <div class="news-update-title">
                        <p>BERITA TERBARU</p>
                        <div class="news-update-underline"></div>
                    </div>
                    <div class="news-update-content">
                        <?php
                        foreach ($news as $item) {
                        ?>
                            <a href="<?php echo base_url() ?>User/newsDetail/<?php echo $item->id; ?>">
                                <div class="news-update-list">
                                    <img class="news-update-image rounded-circle" src="<?php echo base_url('assets/images/' . $item->image) ?>" alt="">
                                    <p><?php echo $item->title; ?></p>
                                </div>
                            </a>
                        <?php
                        } ?>
                    </div>
                    <div class="text-center">
                        <div class="news-update-underline-bottom"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


</html>