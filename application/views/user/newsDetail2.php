<html>
<?php $this->load->view('user/layout/style.php') ?>

<body>

    <div class="news-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="news-detail-content">
                        <img class="news-detail-image" src="<?php echo base_url('assets/user/') ?>images/news-2.webp" alt="">
                        <div class="news-detail-content-information">
                            <img class="news-detail-content-icon" src="<?php echo base_url('assets/user/') ?>images/time-icon.png" alt="">
                            <p>Selasa, 02 Jul 2019, 19:00 WIB</p>
                        </div>

                        <div class="news-detail-content-title">
                            <p>Wisata Gunung Pancar dengan Alam yang Sangat Asri, Cocok Sebagai Pelepas Penat</p>
                        </div>
                        <div class="news-detail-content-paragraph">
                            <p>Jakarta Wisata Gunung Pancar ternyata ada di Bogor. Bogor memang dikenal sebagai kota yang banyak menyimpan potensi alam yang perlu dilestarikan.</p>
                            <p>Banyaknya pegunungan di wilayah Bogor ini, menjadikan kawasan ini memiliki hawa yang sejuk. Maka tak heran kalau banyak orang Bogor dan sekitarnya yang berwisata ke kota ini bersama keluarganya.</p>
                            <p>Tak hanya sejuk, Kota Bogor juga memiliki beragam objek wisata yang patut untuk dikunjungi di akhir pekan. Salah satunya adalah objek wisata Gunung Pancar. Tempat wisata Gunung Pancar ini memang jauh dari pusat kota Bogor. Namun tak ada salahnya untuk memilih berwisata ke Gunung Pancar untuk melepaskan penat selama berkegiatan seminggu penuh.</p>
                            <p>Nah buat kamu yang memiliki rencana untuk menghabiskan akhir pekan di daerah Bogor, wajib memasukkan wisata Gunung Pancar ini ke dalam daftar destinasimu. </p>
                            <h5>Tempat Wisata yang Murah Meriah</h5>
                            <p>Kawasan Taman Wisata Alam (TWA) gunung Pancar yang terletak di Desa Karang Tengah, Kecamatan Babakan Madang, Kabupaten Bogor ini bisa kamu jadikan sebagai tempat liburan yang tepat bila kamu hanya memiliki dana pas-pasan.</p>
                            <p>Namun perlu diperhatikan, saat berkunjung ke sini, kamu perlu menjaga tata krama. Seperti gunung atau hutan lainnya, yang banyak menyimpan misteri.</p>
                            <h5>Melakukan Kegiatan Seru di Gunung Pancar</h5>
                            <p>Wisata Gunung Pancar pertama yang bisa kamu nikmati adalah dengan mencoba beragam kegiatam outdoor. Ya, Gunung Pancar memang sering dipakai untuk kegiatan outbound, camping, gathering, pelatihan kepemimpinan, dan kegiatan outdoor lainnya. Nah, kalau kamu mencari tempat camping di Bogor yang tidak jauh dari Jakarta dan ramah akan keluarga, makaGunung Pancar inilah salah satu pilihan terbaiknya.</p>
                            <p>Tempat ini dijadikan sebagai pilihan yang tepat bagi kamu yang ingin berkemah. Bagaimana tidak, hutan pinus Gunung Pancar Sentul ini menyediakan berbagai fasilitas dan paket-paket yang lengkap.</p>
                            <p>Tak heran, kalau kawasan ini menjadi camping ground terbaik di Bogor karena selalu dirawat oleh para petugas kebersihan. Maka keasrian dan kenyaman kamu dalam melakukan kegiatan di sini selalu terjaga.</p>
                            <h5>Wisata Budaya di Kaki Gunung Pancar</h5>
                            <p>Wisata Gunung Pancar yang bisa kamu nikmati lainnya adalah dengan menyelami budaya masyarakat di sini. Ya, selain menikmati indahnya alam di Gunung Pancar, tak ada salahnya untuk mengenal budaya setempat.</p>
                            <p>Di kawasan wisata Gunung Pancar ini terdapat wisata budaya yang bisa kamu coba. Selain itu, di kawasan Gunung Pancar ini kamu juga bisa berziarah ke makan keramat yang ada. Di puncaknya sendiri, kamu bisa melihat makam-makam keramat yang sudah ada sejak zaman dahulu kala.</p>
                            <p>Ya, terdapat banyak tokoh legenda Jawa yang dimakamkan di wilayah ini. Misalnya saja Raden Lawulung, Ki Mas Bungsu, Raden Surya Kencana, hingga Sunan Kalijaga. Bila kamu datang di waktu yang tepat, maka kamu bisa menyaksikan pertunjukan seni dan upacara keagamaan di daerah ini.</p>
                            <h5>Menikmati Indahnya Alam Sambil Nongkrong</h5>
                            <p>Wisata Gunung Pancar yang bisa dinikmati sambil nongkrong. Ya, siapa sangka di dalam hutan kamu tidak bisa menikmatinya sambil bercengkrama ditemani dengan sajian kopi dan camilan lainnya.</p>
                            <p>Di tengah hutan pinus ini, kamu bisa bersantai sambil ditemani camilan yang tersedia di salah satu café di sini. Ya, di tengah hutan pinus yang tenang dan nyaman ini, kamu bisa menikmati sajian ala café pada umumnya seperi ngopi atau ngeteh.</p>
                            <p>Sudah terbayang kan, bagaimana nikmatnya nongkrong di tengah hutan pinus seperti ini. Tak hanya ditemani dengan camilan yang nikmat, di sini kamu juga bisa mendengar suara-suara kumbang beserta binatang hutan lainnya. Makin seru pastinya.</p>
                            <h5>Nikmati Hangatnya Pemandian Air Panas</h5>
                            <p>Siapa sangka, di wisata Gunung Pancar hanya bisa menikmati indahnya alam saja. Seperti yang sudah diketahui, kalau di Bogor sendiri memiliki beberapa tempat pemandian air panas.</p>
                            <p>Nah, di kawasan Gunung Pancar ini juga ada. Ya, setelah kamu melakukan berbagai kegiatan menantang, kamu bisa langsung menyegarkan diri di pemandian air hangat Giritirta. Di pemandian air hangat ini kamu bisa memilih mau berendam di kolam umum atau pribadi.</p>
                            <h5>Bisa Dijadikan Alternatif untuk Resepsi Pernikahan</h5>
                            <p>Selain dijadikan tempat untuk melakukan berbagai kegiatan outdoor yang menantang, Gunung Pancar ini juga sering digunakan para calon pasangan pengantin untuk mengambil gambar pranikah atau biasa disebut dengan prewedding. Ya, hutan pinus Sentul, merupakan panggilan untuk kawasan wisata ini.</p>
                            <p>Selain kawasannya yang indah dan cocok untuk berswafoto, di sini kamu juga bisa melakukan prosesi pernikahan dengan nuansa alam yang asri. Gunung Pancar ini bisa kamu jadikan alternatif untuk menggelar acara pernikahan. Pohon-pohon pinus di gunung Pancar ini tak kalah indahnya dengan yang ada di Lembang.</p>
                            <p>Ya, memilih hutan pinus untuk prosesi pernikahan, merupakan salah satu tema garden wedding yang sedang populer. Konsep pernikahan rustic outdoor diartikan sebagai konsep pernikahan dengan gaya pedesaan yang elok, karena berkaitan dengan alam dan sederhana. Konsepnya yang menawarkan suasana asri dan alami ini akan memberikan sensasi dan kenangan tersendiri.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-detail-search">
                        <div class="news-detail-search-bar">
                            <input class="news-detail-search-input" type="text" name="" placeholder="Search...">
                            <a href="#" class="">
                                <img class="news-detail-search-icon" src="<?php echo base_url('assets/user/') ?>images/search-icon.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="news-detail-update">
                        <div class="news-detail-update-title">
                            <p>BERITA TERBARU</p>
                            <div class="news-detail-update-underline"></div>
                        </div>
                        <div class="news-update-content">
                            <a href="<?php echo base_url('berita-detail-1') ?>">
                                <div class="news-update-list">
                                    <img class="news-update-image rounded-circle" src="<?php echo base_url('assets/user/') ?>images/news-1.jpg" alt="">
                                    <p>5 Fasilitas di Gunung Pancar, dari Hutan Pinus hingga Pemandian Air Panas</p>
                                </div>
                            </a>
                            <a href="<?php echo base_url('berita-detail-2') ?>">
                                <div class="news-update-list">
                                    <img class="news-update-image rounded-circle" src="<?php echo base_url('assets/user/') ?>images/news-2.webp" alt="">
                                    <p>Wisata Gunung Pancar dengan Alam yang Sangat Asri, Cocok Sebagai Pelepas Penat</p>
                                </div>
                            </a>
                        </div>
                        <div class="text-center">
                            <div class="news-update-underline-bottom"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>