<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="gallery">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="gallery-title">
                    <p>GALLERY</p>
                    <div class="underline-gallery"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
    <?php foreach ($galleries as $i) { 
        ?>
        <div class="col-lg-3 col-md-4 col-sm-6 thumb">
            <div class="gallery-column">
                <a href="<?php echo base_url('assets/images/' . $i->image) ?>" class="fancybox" rel="ligthbox">
                    <img class="zoom img-fluid gallery-image" src="<?php echo base_url('assets/images/' . $i->image) ?>" alt="">
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

</html>

<script>
    $(document).ready(function() {
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });

        $(".zoom").hover(function() {

            $(this).addClass('transition');
        }, function() {

            $(this).removeClass('transition');
        });
    });
</script>