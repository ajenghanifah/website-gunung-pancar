<html>
<?php $this->load->view('user/layout/style.php') ?>
<?php $this->load->view('user/layout/sidebar'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-left: 90px;">
            <form method="post" enctype="multipart/form-data" action="<?php echo base_url('user/konfirmasiUser') ?>">
                <div>
                    <h3 style="margin-bottom: 20px">KONFIRMASI PEMBAYARAN</h3>
                </div>
                <div>
                    <p>Jumlah total yang dibayarkan untuk pesanan ini adalah: Rp. <?php echo number_format($chart_detail->total) ?></p>
                </div>
                <div>
                    <ul>
                        <li>BNI 0804222341 (PT. XXX)</li>
                        <li>BCA 4210244404 (PT. XXX)</li>
                    </ul>
                </div>
                <div>
                    <p style="color: #ff8100;">Harap konfirmasi pembayaran tidak lebih dari 24 Jam dari waktu pembelian</p>
                </div>
                <div class="form-group">
                    <label for="note">Order ID</label>
                    <input readonly type="text" class="form-control" id="note" name="id_chart" value="<?php echo $id_chart ?>">
                </div>
                <div class="form-group">
                    <label for="note">Dekripsi / Catatan</label>
                    <input required type="text" class="form-control" id="note" name="deskripsi" placeholder="Masukkan catatan">
                </div>
                <div class="form-group">
                    <label>Bukti Pembayaran</label>
                    <input required type="file" class="form-control" name="image">
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label text-muted" for="exampleCheck1">DENGAN INI SAYA MENYATAKAN TELAH MELAKUKAN TRANSFER PEMBAYARAN *</label>
                </div>
                <div>
                    <input type="hidden" name="id_produk" value="<?php echo $chart_detail->id_produk ?>">
                    <input type="hidden" name="id_tanggal" value="<?php echo $chart_detail->id_tanggal ?>">
                    <input type="hidden" name="qty" value="<?php echo $chart_detail->qty ?>">
                </div>
                <button type="submit" class="btn btn-secondary btn-block" style="margin: 30px 0px;">Konfirmasi Pembayaran</button>
            </form>
        </div>
    </div>
</div>

</html>