<html>
<?php $this->load->view('user/layout/style.php') ?>
<?php $this->load->view('user/layout/sidebar'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-left: 90px; margin-bottom: 100px">
            <!-- <div class="row"> -->
                <table class="table margin-top-40 table-responsive">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">No</th>
                            <th scope="col">Order ID</th>
                            <th scope="col">Tanggal Transaksi</th>
                            <th scope="col">Items</th>
                            <th scope="col">Total</th>
                            <th scope="col">Pembayaran</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($chart as $key => $charts) { ?>
                            <tr>
                                <th class="text-center" scope="row"><?php echo $key + 1 ?></th>
                                <td class="text-center"> <?php echo $charts->id_chart ?> </td>
                                <td><?php echo $charts->create_chart ?></td>
                                <td><?php echo $charts->nama_produk ?></td>
                                <td>IDR <?php echo number_format($charts->total) ?></td>
                                <td><?php echo $charts->status ?></td>
                                <td>
                                    <?php if ($charts->status === 'USER_KONFIRMASI') { ?>
                                        <button class="btn btn-success">
                                            Sudah Dibayar
                                        </button>
                                    <?php } else if($charts->status === 'MENUNGGU_PEMBAYARAN') { ?>
                                        <a href="<?php echo base_url('user/historyTransactionDetail/') ?><?php echo $charts->id_chart ?>">
                                            <button class="btn btn-primary">
                                                Bayar Sekarang
                                            </button>
                                        </a>
                                    <?php } ?>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <!-- </div> -->
        </div>
    </div>
</div>

<div class="modal fade" id="konfirmasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="konfirmasi">Konfirmasi Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" action="<?php echo base_url('user/konfirmasiUser') ?>">
                    <div class="form-group">
                        <label for="id_chart">Order ID</label>
                        <input readonly type="text" class="form-control" id="id_chart" name="id_chart">
                    </div>
                    <div class="form-group">
                        <label for="note">Dekripsi / Catatan</label>
                        <input required type="text" class="form-control" id="note" name="deskripsi" placeholder="Masukkan catatan">
                    </div>
                    <div class="form-group">
                        <label>Bukti Pembayaran</label>
                        <input required type="file" class="form-control" name="image">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function myFunction(id) {
        // var x = document.getElementById("id_chart_view").value
        document.getElementById("id_chart").value = id
        console.log('X', x)
    }
</script>

</html>