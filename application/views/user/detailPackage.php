<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="package">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="package-title">
                    <p><?php echo $detail->nama_kategori; ?></p>
                    <div class="underline-package"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="detail-package py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div>
                            <!-- <h4>Detail Paket</h4> -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h4><?php echo $detail->nama_produk; ?></h4>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p>Rp. <?php echo number_format($detail->price); ?> / <?php echo $detail->des_price; ?></p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p><?php echo $detail->deskripsi; ?></p>
                            </div>
                        </div>

                        <div class="x_content">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th>No</th>
                                        <th>ID</th>
                                        <th>Tanggal</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($produk as $key => $value) {
                                    ?>
                                        <tr style="text-align: center;">
                                            <td>
                                                <?php echo $no; ?>
                                            </td>
                                            <td>
                                                <input id="id_tanggal" readonly value="<?php echo $value->id; ?>" class="form-control">
                                            </td>
                                            <td>
                                                <input id="tanggal" readonly value="<?php echo $value->tanggal; ?>" class="form-control">
                                            </td>
                                            <td>
                                                <?php
                                                if ($this->session->userdata('email')) { ?>
                                                    <button onclick="myFunction()" class="btn btn-primary" data-toggle="modal" data-target="#pesan">Pesan</button>
                                                <?php } else { ?>
                                                    <a href="<?php echo base_url('login') ?>">
                                                        <button class="btn btn-success">
                                                            Pesan
                                                        </button>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php $no++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="col-lg-12 text-center">
    <div class="sn-business-opportunity-register ">
        <p>Silakan hubungi kami terlebih dahulu untuk jadwal survey dan reservasi</p>
        <a href="<?php echo base_url('contact') ?>">Klik disini!</a>
    </div>
</div>

<div class="modal fade" id="pesan" tabindex="-1" role="dialog" aria-labelledby="pesan" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="pesan">
                    Form Pemesanan Paket
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo base_url('user/postCheckout') ?>">
                    <div class="form-group">
                        <label for="produk">Produk</label>
                        <input class="form-control" type="hidden" name="id_produk" value="<?php echo $detail->id_produk ?>">
                        <input class="form-control" type="hidden" name="id_tanggal" id="tanggal_pilih">
                        <input class="form-control" type="text" readonly value="<?php echo $detail->nama_kategori ?>">
                    </div>
                    <div class="form-group">
                        <label for="tgl">Tanggal</label>
                        <input class="form-control" type="text" readonly id="tanggal_picker">
                    </div>
                    <div class="form-group">
                        <label for="qty">Qty</label>
                        <input required class="form-control" type="number" name="qty" placeholder="0-10">
                    </div>
                    <button type="submit" class="btn btn-primary">Pesan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function myFunction() { 
        var x = document.getElementById("id_tanggal").value;
        document.getElementById("tanggal_pilih").value = x;

        var y = document.getElementById("tanggal").value;
        document.getElementById("tanggal_picker").value = y;
    }
</script>

</html>