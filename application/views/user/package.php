<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="package">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="package-title">
                    <p>BIAYA PAKET DAN TIKET MASUK</p>
                    <div class="underline-package"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="pricing py-5 text-center"> 
    <div class="container">
        <div class="row">
            <?php
            foreach ($super_product as $item) {
                ?>
                <div class="col-lg-4">
                    <a href="<?php echo base_url() ?>User/detailSuperPackage/<?php echo $item->id; ?>" style="text-decoration: none; color:balck">
                        <div class="card" style="margin-bottom: 20px;">
                            <form method="post" action="<?php echo base_url(); ?>user/addCart" method="post" accept-charset="utf-8">
                                <div class="card-body">
                                    <h5 class="card-title text-muted text-uppercase text-center"><?php echo substr($item->nama_kategori, 0, 35); ?></h5>
                                </div>
                            </form>
                        </div>
                    </a>
                </div>
            <?php
            } ?>
        </div>
    </div>
</section>

<div class="col-lg-12 text-center">
    <div class="sn-business-opportunity-register ">
        <p>Silakan hubungi kami terlebih dahulu untuk jadwal survey dan reservasi</p>
        <a href="<?php echo base_url('contact') ?>">Klik disini!</a>
    </div>
    <!-- <div class="sn-business-opportunity-background-register">
        <div>
            <p>Beli Paket Sekarang!</p>
        </div>
        <div class="sn-business-opportunity-button-register">
            <a href="<?php echo base_url('login') ?>">Beli Paket</a>
        </div>
    </div> -->
</div>

</html>