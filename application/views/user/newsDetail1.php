<html>
<?php $this->load->view('user/layout/style.php') ?>

<body>

    <div class="news-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="news-detail-content">
                        <input type="hidden" name="id" value="<?php echo $detail->id ?>">
                        <img class="news-detail-image" src="<?php echo base_url('assets/images/' . $detail->image) ?>" alt="">
                        <div class="news-detail-content-information">
                            <img class="news-detail-content-icon" src="<?php echo base_url('assets/user/') ?>images/time-icon.png" alt="">
                            <p><?php echo $detail->createdAt; ?> WIB</p>
                        </div>

                        <div class="news-detail-content-title">
                            <p><?php echo $detail->title; ?></p>
                        </div>
                        <div class="news-detail-content-paragraph">
                            <p><?php echo $detail->deskripsi; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="news-detail-search">
                        <div class="news-detail-search-bar">
                            <input class="news-detail-search-input" type="text" name="" placeholder="Search...">
                            <a href="#" class="">
                                <img class="news-detail-search-icon" src="<?php echo base_url('assets/user/') ?>images/search-icon.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="news-detail-update">
                        <div class="news-detail-update-title">
                            <p>BERITA TERBARU</p>
                            <div class="news-detail-update-underline"></div>
                        </div>
                        <div class="news-update-content">
                            <?php
                            foreach ($news as $item) {
                                ?>
                                <a href="<?php echo base_url() ?>User/newsDetail/<?php echo $item->id; ?>">
                                    <div class="news-update-list">
                                        <img class="news-update-image rounded-circle" src="<?php echo base_url('assets/images/' . $item->image) ?>" alt="">
                                        <p><?php echo $item->title; ?></p>
                                    </div>
                                </a>
                            <?php
                            } ?>
                        </div>
                        <div class="text-center">
                            <div class="news-update-underline-bottom"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>