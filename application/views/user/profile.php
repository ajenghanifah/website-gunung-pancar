<html>
<?php $this->load->view('user/layout/style.php') ?>
<?php $this->load->view('user/layout/sidebar'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-left: 90px;">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <P class="margin-top-40">Nama</P>
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <P class="margin-top-40"><?php echo $userData->nama ?></P>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <P class="">Nomor Ponsel</P>
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <P class=""><?php echo $userData->telepon ?></P>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <P class="">Email</P>
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <P class="" style="margin-bottom: 200px"><?php echo $userData->email ?></P>
                </div>
            </div>
        </div>
    </div>
</div>


</html>