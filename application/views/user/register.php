<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card card-register">
            <div class="card-header">
                <h3>Daftar Akun</h3>
            </div>
            <div class="card-body">
                <form method="post" action="<?php echo base_url('/User/registerUser') ?>">
                <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Nama" name="nama">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email" name="email">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Telepon" name="telepon">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="Password" name="password">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Register" class="btn btn-block register_btn">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</html>