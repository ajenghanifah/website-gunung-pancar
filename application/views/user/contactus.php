<html>
<?php $this->load->view('user/layout/style.php') ?>

<div class="contact-us">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="">Hubungi Kami</h2>
            </div>
            <div class="col-lg-6">
                <div>
                    <p class="margin-top-40"> <b>TWA Gunung Pancar</b></p>
                    <p class="margin-bottom-0">KP Cibural, Desa Karang Tengah</p>
                    <p class="margin-bottom-0">Kec. Babakan Madang, Kab. Bogor</p>
                    <p class="margin-bottom-0">Jawa Barat, 16810</p>
                    <div class="mapouter margin-top-20">
                        <div class="gmap_canvas">
                            <iframe width="500" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=gunung%20pancar&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                        <style>
                            .mapouter {
                                position: relative;
                                text-align: right;
                                height: 300px;
                                width: 500px;
                            }

                            .gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                                height: 300px;
                                width: 500px;
                            }
                        </style>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <p class="margin-top-40"><b>Contact Person ( 08.00 - 19.00 WIB )</b></p>
                <p class="margin-bottom-0">Informasi: </p>
                <p class="margin-bottom-0"><i class="fa fa-phone" aria-hidden="true"></i></i> 081213446514</p>
                <p class="margin-bottom-0"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@gunungpancar.com </p>
                <p class="margin-top-20 margin-bottom-0">Event Organizer dan Program: </p>
                <p><i class="fa fa-envelope-o" aria-hidden="true"></i> gunungpancarorginizer@gmail.com </p>
                <p class="margin-bottom-0"><b>Mohon hubungi kami terlebih dahulu untuk membuat jadwal survey</b></p>
                <p>Jika anda ingin melakukan survey terlebih dahulu, diwajibkan untuk menghubungi kami terlebih dahulu, agar dapat ditermani oleh marketing kami. Dikarenakan padatnya acara dan pengunjung lainnya yang survey.</p>
            </div>
        </div>

    </div>
</div>


</html>