<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Kategori Produk</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <?php foreach ($CategoryProduct as $i) { ?>
                            <form action="<?php echo base_url() . 'admin/updateCategoryProduct/'; ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $i->id ?>">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Kategori</label>
                                    <input style="text-transform: capitalize;" type="text" class="form-control" name="nama_kategori" value="<?php echo $i->nama_kategori; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Biaya Tambahan</label>
                                    <input type="text" class="form-control" name="biaya_tambahan" value="<?php echo $i->biaya_tambahan; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Deskripsi</label>
                                    <textarea class="form-control" name="deskripsi" rows="3"><?php echo $i->deskripsi; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Upload Gambar</label>
                                    <input name="image" type="file" class="form-control-file <?php echo form_error('image') ? 'is-invalid' : '' ?>" id="image">
                                    <input type="hidden" name="old_image" value="<?php echo $i->image ?>">
                                    <div class="invalid-feedback">
                                        <?php echo form_error('image') ?>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>