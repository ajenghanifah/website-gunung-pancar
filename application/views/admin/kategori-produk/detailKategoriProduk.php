<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Detail Kategori Produk</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <input type="hidden" name="id" value="<?php echo $detail->id ?>">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Nama Kategori</h5>
                                <p class="text-muted"><?php echo $detail->nama_kategori; ?></p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Deskripsi</h5>
                                <p class="text-muted"><?php echo $detail->deskripsi; ?></p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Biaya Tambahan</h5>
                                <p class="text-muted"><?php echo $detail->biaya_tambahan; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>