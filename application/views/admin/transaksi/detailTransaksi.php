<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Detail Transaksi</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Order Id</p>
                                <p><?php echo $transaksi->id_chart ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Tanggal Transaksi</p>
                                <p><?php echo $transaksi->create_chart ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Nama</p>
                                <p><?php echo $transaksi->nama_pelanggan ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>No Ponsel</p>
                                <p><?php echo $transaksi->telepon ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Email</p>
                                <p><?php echo $transaksi->email ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Items</p>
                                <p><?php echo $transaksi->nama_produk ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Tanggal</p>
                                <p><?php echo $transaksi->tanggal ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Quantity</p>
                                <p><?php echo $transaksi->qty ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Total Pembayaran</p>
                                <p>Rp. <?php echo number_format($transaksi->total) ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Status</p>
                                <p><?php echo $transaksi->status ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Bukti Pembayaran</p>
                                <p><img src="<?php echo base_url('assets/img/konfirmasi/') ?><?php echo $transaksi->image ?>" class="img-thumbnail" /></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Konfirmasi
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modaltolak">
                                Tolak
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="<?php echo base_url('admin/konfirmasiByAdmin') ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                        </div>
                        <div class="modal-body">
                            Apakah Anda yakin ingin melakukan konfirmasi pembayaran?
                            <div>
                                <input type="hidden" name="id_chart" value="<?php echo $transaksi->id_chart ?>">
                                <input type="hidden" name="id_user" value="<?php echo $transaksi->id_user ?>">
                                <input type="hidden" name="id_produk" value="<?php echo $transaksi->id_produk ?>">
                                <input type="hidden" name="id_tanggal" value="<?php echo $transaksi->id_tanggal ?>">
                                <input type="hidden" name="qty" value="<?php echo $transaksi->qty ?>">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Ya, Konfirmasi</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="modaltolak" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form method="post" action="<?php echo base_url('admin/tolakByAdmin') ?>">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tolak Pembayaran</h5>
                        </div>
                        <div class="modal-body">
                            Apakah Anda yakin ingin melakukan tolak pembayaran?
                            <div>
                                <input type="hidden" name="id_chart" value="<?php echo $transaksi->id_chart ?>">
                                <input type="hidden" name="id_user" value="<?php echo $transaksi->id_user ?>">
                                <input type="hidden" name="id_produk" value="<?php echo $transaksi->id_produk ?>">
                                <input type="hidden" name="id_tanggal" value="<?php echo $transaksi->id_tanggal ?>">
                                <input type="hidden" name="qty" value="<?php echo $transaksi->qty ?>">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-danger">Ya, Tolak</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>