<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Transaksi</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Transaksi</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Order Id</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama</th>
                                    <th>Total Pembayaran</th>
                                    <th>Status</th>
                                    <th>Bukti Bayar</th>
                                    <th class="text-center">-</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($transaksi as $key => $transaksis) { ?>
                                    <tr>
                                        <td class="py-1"><?php echo $key + 1 ?></td>
                                        <td><?php echo $transaksis->id_chart ?></td>
                                        <td><?php echo $transaksis->create_chart ?></td>
                                        <td><?php echo $transaksis->nama_pelanggan ?></td>
                                        <td>Rp. <?php echo number_format($transaksis->total) ?></td>
                                        <td <?php if ($transaksis->status === 'ORDER_DITOLAK') {
                                                echo 'style="color: red;"';
                                            } else {
                                                echo 'style="color: green;"';
                                            } ?>>
                                            <?php echo $transaksis->status ?>
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url('assets/img/konfirmasi/') ?><?php echo $transaksis->image ?>" style="width: 100px;" />
                                        </td>
                                        <td>
                                            <?php if ($transaksis->status === 'ORDER_DITOLAK' || $transaksis->status === 'KONFIRMASI_BY_ADMIN') { ?>
                                                <!-- echo 'style="color: red;"'; -->
                                                <button disabled class="btn btn-primary btn-block">
                                                    Aksi
                                                </button>
                                            <?php } else { ?>
                                                <!-- echo 'style="color: green;"'; -->
                                                <a href="<?php echo base_url('admin/detailTransaksi/') ?><?php echo $transaksis->id_chart ?>">
                                                    <button class="btn btn-primary btn-block">
                                                        Aksi
                                                    </button>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalkonfirmasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tindakan untuk pembayaran ini</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <p>Order ID : 3</p>
                        <p>Nama : Iqbal</p>
                        <p>Total : Rp <?php echo number_format(500000) ?></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tolak</button>
                    <button type="button" class="btn btn-primary">Konfirmasi</button>
                </div>
            </form>
        </div>
    </div>
</div>