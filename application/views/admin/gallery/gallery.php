<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gallery</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Gallery</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <a href="<?php echo base_url() ?>admin/addGallery" class="btn btn-sm btn-primary">
                            <i class="mdi mdi-plus"></i> Tambah Gallery</a>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Deskripsi</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($galleries as $i) {
                                ?>
                                    <tr>
                                        <td class="py-1">
                                            <?php echo $no; ?>
                                        </td>
                                        <td><?php echo substr($i->title, 0, 35); ?></td>
                                        <td><?php echo substr($i->description, 0, 35); ?></td>
                                        <td>
                                            <img src="<?php echo base_url('assets/images/' . $i->image) ?>" style="width:150px; height:150px" />
                                        </td>
                                        <td style="display: flex; flex-direction: column; justify-content: space-around;">
                                            <a style="margin-bottom:5px" href="<?php echo base_url() ?>admin/editGallery/<?php echo $i->id; ?>" class="btn btn-sm btn-primary  ">
                                                 Edit</a>
                                            <a style="margin-bottom:5px" href="<?php echo base_url() ?>admin/deleteGallery/<?php echo $i->id; ?>" class="btn btn-sm btn-danger tombol-hapus">
                                                Delete</a>
                                        </td>
                                    </tr>
                                <?php $no++;
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>