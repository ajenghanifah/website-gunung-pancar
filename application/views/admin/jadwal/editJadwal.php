<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Produk</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <?php foreach ($Schedule as $i) { ?>
                            <form action="<?php echo base_url() . 'admin/updateSchedule/'; ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $i->id ?>">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Produk</label>
                                    <select name="id_produk" class="form-control">
                                        <?php foreach ($produk as $item) : ?>
                                            <option value="<?php echo $item->id ?>" <?php if ($item->id == $i->id_produk) echo 'selected'; ?>>
                                                <?php echo $item->nama_produk ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tanggal</label>
                                    <input name="tanggal" style="text-transform: capitalize;" type="date" class="form-control" value="<?php echo $i->tanggal; ?>">
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>