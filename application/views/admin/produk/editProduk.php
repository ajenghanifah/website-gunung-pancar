<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Produk</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <?php foreach ($product as $i) { ?>
                            <form action="<?php echo base_url() . 'admin/updateProduct/'; ?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $i->id ?>">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kategori Produk</label>
                                    <select name="id_kategori" class="form-control">
                                        <?php foreach ($kategori_produk as $item) : ?>
                                            <option value="<?php echo $item->id ?>" <?php if ($item->id == $i->id_kategori) echo 'selected'; ?>>
                                                <?php echo $item->nama_kategori ?>
                                            </option>
                                        <?php endforeach; ?> 
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Produk</label>
                                    <input name="nama_produk" style="text-transform: capitalize;" type="text" class="form-control" value="<?php echo $i->nama_produk; ?>">
                                </div>
                                <div class="form">
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Harga</label>
                                        <input name="price" type="text" class="form-control" value="<?php echo $i->price; ?>">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Keterangan Harga</label>
                                        <input name="des_price" type="text" class="form-control" value="<?php echo $i->des_price; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Deskripsi</label>
                                    <textarea name="deskripsi" class="form-control" rows="3"><?php echo $i->deskripsi; ?></textarea>
                                </div>
                                <!-- <div class="form-group">
                                <label for="exampleInputEmail1">Harga</label>
                                <input type="text" class="form-control" name="image" placeholder="Masukkan Harga">
                            </div> -->
                                <!-- <div class="form-group">
                                <label for="exampleFormControlFile1">Upload Gambar</label>
                                <input type="file" class="form-control-file" id="image">
                            </div> -->
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>