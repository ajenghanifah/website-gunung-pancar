<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Detail Produk</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <input type="hidden" name="id" value="<?php echo $detail->id ?>">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Kategori Produk</p>
                                <p><?php echo $detail->nama_kategori; ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Nama Produk</p>
                                <p><?php echo $detail->nama_produk; ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Harga</p>
                                <p><?php echo $detail->price; ?></p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <p>Deskripsi</p>
                                <p><?php echo $detail->deskripsi; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>