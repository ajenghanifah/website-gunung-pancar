<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Tambah Produk</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form action="<?php echo base_url() . 'admin/createProduct/'; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kategori Produk</label>
                                <select name="id_kategori" class="form-control">
                                    <?php foreach ($kategori_produk as $item) : ?>
                                        <option value="<?php echo $item->id ?>"><?php echo $item->nama_kategori ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Produk</label>
                                <input style="text-transform: capitalize;" type="text" class="form-control" name="nama_produk" placeholder="Masukkan Nama Produk">
                            </div>
                            <div class="form">
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Harga</label>
                                    <input type="text" class="form-control" name="price" placeholder="Masukkan Harga">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputEmail1">Keterangan Harga</label>
                                    <input type="text" class="form-control" name="des_price" placeholder="Masukkan Keterangan Harga">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" rows="3"></textarea>
                            </div>
                            <!-- <div class="form-group">
                                <label for="exampleInputEmail1">Harga</label>
                                <input type="text" class="form-control" name="image" placeholder="Masukkan Harga">
                            </div> -->
                            <!-- <div class="form-group">
                                <label for="exampleFormControlFile1">Upload Gambar</label>
                                <input type="file" class="form-control-file" id="image">
                            </div> -->
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>