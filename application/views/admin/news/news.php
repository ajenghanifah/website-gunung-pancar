<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Berita</h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>List Berita</h2>
            <div class="clearfix"></div>
          </div>
          <div>
            <a href="<?php echo base_url() ?>admin/addNews" class="btn btn-sm btn-primary">
              <i class="mdi mdi-plus"></i> Tambah Berita</a>
          </div>
          <div class="x_content">
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Gambar</th>
                  <th>Waktu</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($news->result() as $item) {
                ?>
                  <tr>
                    <td class="py-1">
                      <?php echo $no; ?>
                    </td>
                    <td><?php echo substr($item->title, 0, 35); ?></td>
                    <td>
                      <img src="<?php echo base_url('assets/images/' . $item->image) ?>" style="width:150px; height:150px" />
                    </td>
                    <td><?php echo substr($item->createdAt, 0, 35); ?></td>
                    <td style="display: flex; flex-direction: column; justify-content: space-around;">
                      <a style="margin-bottom:5px" href="<?php echo base_url() ?>admin/detailNews/<?php echo $item->id; ?>" class="btn btn-sm btn-primary  ">
                        Detail</a>
                      <a style="margin-bottom:5px" href="<?php echo base_url() ?>admin/editNews/<?php echo $item->id; ?>" class="btn btn-sm btn-primary  ">
                        Edit</a>
                      <a style="margin-bottom:5px" href="<?php echo base_url() ?>admin/deleteNews/<?php echo $item->id; ?>" class="btn btn-sm btn-danger tombol-hapus">
                        Delete</a>
                    </td>
                  </tr>
                <?php $no++;
                } ?>
              </tbody>
              <div class="container">
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>