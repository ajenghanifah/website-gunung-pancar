<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Detail Berita</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <input type="hidden" name="id" value="<?php echo $detail->id ?>">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Judul</h5>
                                <p class="text-muted"><?php echo $detail->title; ?></p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Deskripsi</h5>
                                <p class="text-muted"><?php echo $detail->deskripsi; ?></p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Gambar</h5>
                                <img src="<?php echo base_url('assets/images/' . $detail->image) ?>" style="max-width:100%; " />
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <h5>Tanggal</h5>
                                <p class="text-muted"><?php echo $detail->createdAt; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>