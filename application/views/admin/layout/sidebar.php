<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo base_url('admin/index'); ?>" class="site_title"><i class="fa fa-paw"></i> <span>Wisata Gunung Pancar</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="<?php echo base_url('assets/user/images/admin.png') ?>" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2>Admin</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="<?php echo base_url('admin/index'); ?>">
                            <i class="fa fa-home"></i> Dasbor
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/transaksi'); ?>">
                        <i class="fa fa-shopping-cart"></i> Transaksi
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/categoryProduct'); ?>">
                            <i class="fa fa-tree"></i> Kategori Produk
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/product'); ?>">
                            <i class="fa fa-ticket"></i> Produk
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/Schedule'); ?>">
                            <i class="fa fa-tree"></i> Jadwal Produk
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/gallery'); ?>">
                            <i class="fa fa-picture-o"></i> Gallery
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/news'); ?>">
                            <i class="fa fa-newspaper-o"></i> Berita
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>